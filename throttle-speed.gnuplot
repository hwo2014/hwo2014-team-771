set terminal png medium size 800, 500
set output "throttle-speed.png"

set style data lines

set xlabel "tick"

set grid x y

set y2tics

set border
plot 'run.data' using 1:5 t 'curve', \
	 'run.data' using 1:3 t 'speed', \
     'run.data' using 1:4 t 'throttle', \
	 'run.data' using 1:6 axes x1y2 t 'angle'
#	 'run.data' using 1:2 axes x1y2 t 'piece', \

