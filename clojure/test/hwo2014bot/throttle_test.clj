(ns hwo2014bot.throttle-test
  (:require [clojure.test :refer :all]
            [hwo2014bot.throttle :refer :all]
            [hwo2014bot.test-data :as test-data]))

; data from https://helloworldopen.com/techspec#5-server-sends-carpositions-
(def car-positions
  [{:piecePosition {:inPieceDistance 0.0, :lane {:endLaneIndex 0, :startLaneIndex 0}, :pieceIndex 0, :lap 0},
    :angle 0.0, :id {:name "Schumacher", :color "red"}}
   {:piecePosition {:inPieceDistance 20.0, :lane {:endLaneIndex 1, :startLaneIndex 1}, :pieceIndex 0, :lap 0},
    :angle 89.0, :id {:name "Rosberg", :color "blue"}}])

(def extreme-positions
  [{:piecePosition
    {:pieceIndex 0, :inPieceDistance 0.0, :lane {:endLaneIndex 0, :startLaneIndex 0}},
    :angle -89, :id {:name "Michi"}}
   {:piecePosition
    {:pieceIndex 0, :inPieceDistance 1.0 :lane {:endLaneIndex 1, :startLaneIndex 1}},
    :angle 0, :id {:name "Keke"}}
   {:piecePosition
    {:pieceIndex 0, :inPieceDistance 10.0 :lane {:startLaneIndex 0, :endLaneIndex 0}},
    :angle 89, :id {:name "JJ"}}])

(def track
  {:pieces [{:length 100, :switch true, :lanes '({:length 100.0, :curve 0, :index 0, :distanceFromCenter -10} {:length 100.0, :curve 0, :index 1, :distanceFromCenter 10})}
            {:radius 100, :angle 45.0, :lanes '({:length 47.12388980384689, :curve 0.9549296585513721, :index 0, :distanceFromCenter -10} {:length 31.41592653589793, :curve 1.432394487827058, :index 1, :distanceFromCenter 10})}
            {:radius 100, :angle 45.0, :lanes '({:length 47.12388980384689, :curve 0.9549296585513721, :index 0, :distanceFromCenter -10} {:length 31.41592653589793, :curve 1.432394487827058, :index 1, :distanceFromCenter 10})}]})

(defn- get-position
  [positions botname]
  (first (filter #(= (-> % :id :name) botname) positions)))


(def pieces (:pieces track))

(defn gen-position
  ([index]
     (gen-position index 0 0 0))
  ([index in-piece-dist start-lane-idx end-lane-idx]
     {:piecePosition
      {:pieceIndex index :inPieceDistance in-piece-dist
       :lane {:startLaneIndex start-lane-idx :endLaneIndex end-lane-idx}}}))

(deftest test-on-straight
  (testing "current"
    (is (on-straight? pieces (gen-position 0)))
    (is (not (on-straight? pieces (gen-position 1)))))
  (testing "previous"
    (is (not (on-straight? pieces (gen-position 0) -1)))
    (is (on-straight? pieces (gen-position 1) -1))
    (is (not (on-straight? pieces (gen-position 2) -1)))))


(deftest test-on-bend
  (testing "current"
    (is (on-bend? pieces (gen-position 1)))
    (is (not (on-bend? pieces (gen-position 0)))))
  (testing "previous"
    (is (on-bend? pieces (gen-position 0) -1))
    (is (not (on-bend? pieces (gen-position 1) -1)))
    (is (on-bend? pieces (gen-position 2) -1))))

(deftest test-get-corners-max-speeds-at-distance
  (testing "corner distance"
    (let [{d :distance m :max-speed}
          (first (get-corners-max-speeds-at-distance track (gen-position 0 50.0 0 0) 300))]
      (is (= 50.0 d)))))

(deftest test-need-to-brake
  (testing "need to brake"
    (is (need-to-brake 15.0 '({:distance 1 :max-speed 1}) 1))
    (is (not (need-to-brake 5.0 '({:distance 1 :max-speed 10}) 1)))))

(deftest test-home-straight?
  (testing "home straight in keimola"
    (let [race-keimola {:track test-data/track-keimola :raceSession {:laps 2}}
          first-lap {:piecePosition {:lap 0 :pieceIndex 35}}
          last-lap-almost {:piecePosition {:lap 1 :pieceIndex 34}}
          last-lap-home {:piecePosition {:lap 1 :pieceIndex 35}}]
      (is (home-straight? race-keimola last-lap-home))
      (is (not (home-straight? race-keimola last-lap-almost)))
      (is (not (home-straight? race-keimola first-lap)))))
  (testing "home straight in germany")
    (let [race-germany {:track test-data/track-germany :raceSession {:laps 2}}
          first-lap {:piecePosition {:lap 0 :pieceIndex 54}}
          last-lap-almost {:piecePosition {:lap 1 :pieceIndex 53}}
          last-lap-home {:piecePosition {:lap 1 :pieceIndex 54}}]
      (is (home-straight? race-germany last-lap-home))
      (is (not (home-straight? race-germany last-lap-almost)))
      (is (not (home-straight? race-germany first-lap)))))
