(ns hwo2014bot.race-test
  (:require [clojure.test :refer :all]
            [hwo2014bot.race :refer :all]
            [hwo2014bot.test-data :refer :all]))


(deftest test-piece-lane-length
  (testing "Straight piece"
    (is (= (piece-lane-length {:length 100} {:distanceFromCenter 0}) 100)))
  (testing "Straight piece with distance"
    (is (= (piece-lane-length {:length 100} {:distanceFromCenter 0.5}) 100)))
  (testing "Left curve"
    (is (= (piece-lane-length {:radius 1 :angle -180} {:distanceFromCenter 0}) Math/PI)))
  (testing "Right curve"
    (is (= (piece-lane-length {:radius 1 :angle 180} {:distanceFromCenter 0}) Math/PI)))
  (testing "Left curve with distance"
    (is (= (piece-lane-length {:radius 0.5 :angle -180} {:distanceFromCenter 0.5}) Math/PI)))
  (testing "Right curve with distance"
    (is (= (piece-lane-length {:radius 1.5 :angle 180} {:distanceFromCenter 0.5}) Math/PI))))
