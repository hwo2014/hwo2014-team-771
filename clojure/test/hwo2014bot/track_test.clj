(ns hwo2014bot.track-test
  (:require [clojure.test :refer :all]
            [hwo2014bot.track :refer :all]
            [hwo2014bot.test-data :refer :all]))


(deftest test-distance
  ;; TODO: Add tests for curves, add tests for single switch
  (testing "Single piece distance"
    (is (= 50.0 (distance track-keimola
                          {:pieceIndex 0
                           :inPieceDistance 10.0
                           :lane {:startLaneIndex 0
                                  :endLaneIndex 0}
                           :lap 0}
                          {:pieceIndex 0
                           :inPieceDistance 60.0
                           :lane {:startLaneIndex 0
                                  :endLaneIndex 0}
                           :lap 0}))))
  (testing "Double piece distance"
    (is (= 100.0 (distance track-keimola
                           {:pieceIndex 0
                            :inPieceDistance 0
                            :lane {:startLaneIndex 0
                                   :endLaneIndex 0}
                            :lap 0}
                           {:pieceIndex 1
                            :inPieceDistance 0
                            :lane {:startLaneIndex 0
                                   :endLaneIndex 0}
                            :lap 0}))))
  (testing "Double piece distance with inPieceDistance"
    (is (= 110.0 (distance track-keimola
                           {:pieceIndex 0
                            :inPieceDistance 30.0
                            :lane {:startLaneIndex 0
                                   :endLaneIndex 0}
                            :lap 0}
                           {:pieceIndex 1
                            :inPieceDistance 40.0
                            :lane {:startLaneIndex 0
                                   :endLaneIndex 0}
                            :lap 0}))))
  (testing "Distance over finish line"
    (is (= 150.0 (distance track-keimola
                           {:pieceIndex 39
                            :inPieceDistance 40.0
                            :lane {:startLaneIndex 0
                                   :endLaneIndex 0}
                            :lap 0}
                           {:pieceIndex 1
                            :inPieceDistance 0
                            :lane {:startLaneIndex 0
                                   :endLaneIndex 0}
                            :lap 0})))))

(deftest test-final-lap?
  (testing "final lap"
    (is (final-lap? {:raceSession {:laps 2}} {:piecePosition {:lap 1}}))
    (is (not (final-lap? {:raceSession {:laps 3}} {:piecePosition {:lap 1}})))
    (is (not (final-lap? {:raceSession {:laps 3}} {:piecePosition {:lap 0}})))))
