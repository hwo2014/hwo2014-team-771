(ns hwo2014bot.state
  (:require [hwo2014bot.race :as race]
            [hwo2014bot.track :as track]
            [hwo2014bot.route :as route]
            [hwo2014bot.throttle :as throttle]
            [taoensso.timbre :as timbre]))

(def ^:private car (atom {}))
(def ^:private race (atom {}))
(def ^:private status
  (atom {:tick 0
         :position {}
         :angle 0
         :speed 0
         :throttle 0
         :switch-lane nil
         :switch-changed false}))
(def ^:private turbo
  (atom {:turbo-factor 1
         :turbo-available false
         :turbo-duration 0
         :turbo-usable-till-tick 0}))

(defn- get-tick []
  (:tick @status))

(defn straight-ahead? [position track]
  ;; let's say it's good to use turbo when at straight and the next
  ;; curve is at least 350 units away.
  (and
   (throttle/on-straight? (:pieces track) position)
   (empty? (throttle/get-corners-max-speeds-at-distance track position 350))))

(defn save-turbo
  [turbo-available turbo-factor turbo-duration]
  (timbre/info "Saving turbo" turbo-factor "for" turbo-duration "ticks, available from" (get-tick))
  (reset! turbo {:turbo-factor turbo-factor
                 :turbo-available turbo-available
                 :turbo-duration turbo-duration
                 :turbo-in-use false
                 :turbo-on-till-tick (:turbo-on-till-tick @turbo)}))

(defn current-turbo-factor []
  (if (:turbo-in-use @turbo) (:turbo-factor @turbo) 1))

(defn use-turbo? []
  (and (:turbo-available @turbo)
       (straight-ahead? (:position @status) (:track @race))
       (reset! turbo {:turbo-factor (:turbo-factor @turbo)
                      :turbo-available false
                      :turbo-in-use true
                      :turbo-on-till-tick (+ (:turbo-duration @turbo) (get-tick))
                      :turbo-duration 0})))

(def ^:private piece-speeds (atom []))

(defn- clear-piece-speeds
  []
  (reset! piece-speeds []))

  (defn- add-piece-speed
    [speed]
    (swap! piece-speeds conj speed))

(defn- get-current-lane
  [piece-position piece]
  (let [start-lane (-> piece-position :lane :startLaneIndex)
        end-lane (-> piece-position :lane :endLaneIndex)
        in-piece-distance (:inPieceDistance piece-position)
        lane-length (-> piece :lanes (nth start-lane) :length)]
    (if (= start-lane end-lane)
      start-lane
      (if (> in-piece-distance (/ lane-length 2))
        end-lane
        start-lane))))

(defn- update-status
  [status tick our-position their-positions options]
  (let [track (:track @race)
        lastpos (-> status :position :piecePosition)
        currpos (-> our-position :piecePosition)
        distance (track/distance track lastpos currpos)
        tickdiff (- tick (:tick status))
        new-piece (not= (:pieceIndex lastpos) (:pieceIndex currpos))
        speed (if (pos? tickdiff) (/ distance tickdiff) 0)
        new-throttle (throttle/get-throttle @race our-position their-positions speed (current-turbo-factor))
        pieces (:pieces track)
        curr-piece (nth pieces (:pieceIndex currpos))
        prev-piece (nth pieces (or (:pieceIndex lastpos) (:pieceIndex currpos)))]
    ;; only consider values got from straight pieces, due to
    ;; speedometer and piece length issue
    (when (and (throttle/is-straight? curr-piece)
               (and (> (:throttle status) 0) (< new-throttle 0.01)))
      (throttle/braking-started tick speed))
    (when (and (throttle/is-straight? prev-piece)
               (throttle/is-bend? curr-piece))
      (throttle/braking-ended (- tick 1) (:speed status)))
    (when (and (throttle/is-straight? curr-piece)
               (< (:throttle status) new-throttle))
      (throttle/braking-ended tick speed))

    (when (and (:turbo-in-use @turbo) (> (get-tick) (:turbo-on-till-tick @turbo)))
      (save-turbo false 1 0))

    (when (and (:debug options) (pos? tickdiff))
      (if (and new-piece
               (not (empty? @piece-speeds)))
        (let [max-speed (apply max @piece-speeds)
              piece-count (-> track :pieces count)
              piece-index (+ (* (-> our-position :piecePosition :lap) piece-count)
                             (:pieceIndex currpos) -1)]
          (clear-piece-speeds)
          (timbre/debug "Max-speed" piece-index max-speed))
        (add-piece-speed speed))

      (let [piece-index (:pieceIndex currpos)
            current-piece (-> track :pieces (nth piece-index))
            lane (get-current-lane currpos current-piece)]
        (timbre/info tick piece-index speed new-throttle (-> current-piece :lanes (nth lane) :curve)
                     (:angle status))))

    (merge {:tick tick
            :position our-position
            :angle (:angle our-position)
            :speed speed
            :throttle new-throttle
            :switch-lane (:switch-lane status)
            :switch-lane-changed false}
           (when (and new-piece (or (:switch prev-piece) (not (:pieceIndex lastpos))))
             (let [switch-lane (route/switch-lane @race our-position their-positions)]
               (timbre/info "Executed switch lane logic with result" switch-lane)
               {:switch-lane switch-lane
                :switch-lane-changed (not= switch-lane (:switch-lane status))})))))

(defn get-status
  []
  @status)

(defn save-car
  [new-car]
  (reset! car new-car))

(defn get-car
  []
  @car)

(defn save-race
  [new-race]
  (reset! race (race/process new-race)))

(defn get-race
  []
  @race)

(defn update-position
  [new-tick new-positions options]
  (let [tick (if new-tick new-tick 0)
        our-car #(= (-> % :id :color) (:color @car))
        our-position (first (filter our-car new-positions))
        their-positions (remove our-car new-positions)]
    (swap! status update-status tick our-position their-positions options)))
