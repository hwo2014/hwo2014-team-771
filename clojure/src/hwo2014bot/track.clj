(ns hwo2014bot.track
  (:require [taoensso.timbre :as timbre]
            [clojure.math.numeric-tower :as math]))


(def switch-penalty 2)


(def ^:dynamic *exclude-paths* [])
(def ^:dynamic *max-switches* 1)

(defn- recurse-pieces
  ([pieces laneidx]
   (recurse-pieces pieces laneidx "" 0))
  ([pieces laneidx path distance]
   (if
     (or (nil? (first pieces))
         (and (:switch (first pieces))
              (>= *max-switches* 0)
              (< *max-switches* (count path))))
     [[path distance]]
     (let [piece (first pieces)
           length (+ distance (-> piece :lanes (nth laneidx) :length))
           switch-length (+ length switch-penalty)]
       (if (:switch piece)
         (concat
           (when (> laneidx 0)
             (recurse-pieces (rest pieces) (dec laneidx) (str path \l) switch-length))
           (when (< laneidx (- (count (:lanes piece)) 1))
             (recurse-pieces (rest pieces) (inc laneidx) (str path \r) switch-length))
           ;; Keep this the last, it will promote early lane switching
           (recurse-pieces (rest pieces) laneidx (str path \s) length))
         (recur (rest pieces) laneidx path length))))))

(defn get-pieces
  "Get pieces of a single lap starting from startidx, if final-lap
  is defined then the pieces after finish line are not included."
  ([track]
   (get-pieces track 0 false))
  ([track startidx]
   (get-pieces track startidx false))
  ([track startidx final-lap]
   (concat
     (drop startidx (:pieces track))
     (when (not final-lap)
       (take startidx (:pieces track))))))

(defn shortest-paths
  "Calculate the shortest path starting from pieceidx on lane
  laneidx. Currently if there are multiple paths, selects one
  randomly. Returns the path in format [\"lssr\" 2520.0] where
  \"lssr\" means the path (left-straight-straight-right) and
  2520.0 is the total length of the selected path."
  [track pieceidx laneidx final-lap]
  (let [pieces (get-pieces track pieceidx final-lap)
        data (binding [*max-switches* (count (:lanes track))]
               (recurse-pieces pieces laneidx))]
    (sort-by second data)))

(defn piece-length
  [piece startlane endlane]
  (let [startlen (-> piece :lanes (nth startlane) :length)
        endlen (-> piece :lanes (nth endlane) :length)
        avglen (+ (/ startlen 2) (/ endlen 2))
        lanedist (math/abs (- (-> piece :lanes (nth startlane) :distanceFromCenter)
                              (-> piece :lanes (nth endlane) :distanceFromCenter)))]
    (if (and (not= startlane endlane) (not (:angle piece)))
      ;; For straight switch piece use pythagoras
      (math/sqrt (+ (math/expt avglen 2) (math/expt lanedist 2)))
      avglen))) ; Otherwise use average length

(defn distance
  "Calculates the distance between startpos and endpos, both of
  which are piecePositions received in carPositions message."
  [track startpos endpos]
  (if (or (nil? startpos) (nil? endpos))
    0.0
    (let [piece-diff (- (:pieceIndex endpos) (:pieceIndex startpos))
          piece-count (if (neg? piece-diff)
                        (+ piece-diff (count (:pieces track)))
                        piece-diff)
          pieces (get-pieces track (:pieceIndex startpos))
          first-piece (first pieces)
          mid-pieces (take (- piece-count 1) (rest pieces))
          startlane (-> startpos :lane :startLaneIndex)
          endlane (-> startpos :lane :endLaneIndex)]
      ;; Make sure we have calculated the lane lengths correctly, warn otherwise
      (when (> (:inPieceDistance startpos) (piece-length first-piece startlane endlane))
        (timbre/warn "Position too far" startpos (first pieces)))
      ;; This will break if we have many switches between two positions, should be ok...
      (reduce + (apply list (- (:inPieceDistance startpos))
                            (:inPieceDistance endpos)
                            (if (> piece-count 0)
                              (piece-length first-piece startlane endlane)
                              0)
                            (map #(piece-length % endlane endlane) mid-pieces))))))


(defn final-lap?
  [race our-position]
  (if-not (-> race :raceSession :laps)
    true
    (= (-> our-position :piecePosition :lap)
       (- (-> race :raceSession :laps) 1))))
