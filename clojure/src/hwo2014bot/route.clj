(ns hwo2014bot.route
  (:require [taoensso.timbre :as timbre]
            [hwo2014bot.track :as track]))

(defn switch-lane
  [race our-position their-positions]
  (let [piece-idx (-> our-position :piecePosition :pieceIndex)
        lane-idx (-> our-position :piecePosition :lane :startLaneIndex)
        final-lap (track/final-lap? race our-position)
        paths (track/shortest-paths (:track race) piece-idx lane-idx final-lap)
        selected (rand-nth (take (* 1.5 (count (-> race :track :lanes))) paths))
        direction (ffirst selected)]
    ;; Do not switch lane if already on the switch piece
    (if (-> race :track :pieces (nth piece-idx) :switch)
      nil
      (case direction
        \l "Left"
        \r "Right"
        nil))))



