(ns hwo2014bot.throttle
  (:require [taoensso.timbre :as timbre]
            [hwo2014bot.track :as track]))

(def max-throttle 1.0)
(def min-throttle 0.0)

(def friction (atom 0.3))
(def max-angle (atom 75.0))
(def downforce (atom 0.25))

;;; Maximal mean value of speed of each successfully taken corner so far.
(def piece-speed-memory (atom {}))
;;; curr-mem-max-length: take this many samples of each corner, keep
;;; only fastest ones. This way regaining speed should be faster.
(def curr-mem-max-length 8)
(def piece-speed-memory-curr-entry (atom {:samples (list) :invalid false}))

(defn memorize-piece-speed [index speed]
  (timbre/debug "Memorized piece" index "speed" speed)
  (swap! piece-speed-memory assoc index speed))

(defn inc-piece-speed-entry [index]
  (let [{s :samples i :invalid} @piece-speed-memory-curr-entry
        s-len (count s)]
    (when (and (not i) (> s-len 0))
      (let [mean-speed (/ (reduce + s) s-len)
            old-entry (get @piece-speed-memory index)]
        (when (or (not old-entry) (> mean-speed old-entry))
          (memorize-piece-speed index mean-speed))))))

(defn update-piece-speed-entry [speed]
  (let [{s :samples i :invalid} @piece-speed-memory-curr-entry
        s-len (count s)]
    (when (and (not i)
               (or (< s-len curr-mem-max-length)
                   (some #(< % speed) s)))
      ;; Push this value and remove smallest.
      (let [new-samples-all (sort (conj s speed))
            new-samples (if (< s-len curr-mem-max-length)
                          new-samples-all
                          (drop 1 new-samples-all))]
        (reset! piece-speed-memory-curr-entry {:samples new-samples :invalid false})))))

(defn clear-piece-speed-entry []
  (reset! piece-speed-memory-curr-entry {:samples (list) :invalid false}))

(defn mark-piece-speed-entry-invalid []
  (reset! piece-speed-memory-curr-entry {:samples (list) :invalid true}))

(defn get-piece-speed-memory [index]
  (get @piece-speed-memory index))

(defn- get-current-index
  [position]
  (-> position :piecePosition :pieceIndex))

(defn is-straight? [piece]
  (contains? piece :length))

(defn is-bend? [piece]
  (not (is-straight? piece)))

(defn on-straight?
  "Is the piece in position + offset a straight piece? Default offset is zero."
  ([pieces position]
   (on-straight? pieces position 0))
  ([pieces position offset]
  (let [current-index (get-current-index position)
        offset-index (mod (+ current-index offset) (count pieces))
        piece (nth pieces offset-index)]
    (is-straight? piece))))

(defn on-bend?
  "Is the piece in position + offset a bend piece? Default offset is zero."
  ([pieces position]
   (on-bend? pieces position 0))
  ([pieces position offset]
   (not (on-straight? pieces position offset))))

(let [deceleration-coefficient (atom 0.98)
      ;; It is assumed that, when throttle is off, the speed for next
      ;; tick follows a recurrence relation (v[t+1]=cv[t]) where c is
      ;; deceleration coefficient and v[t] is the speed at given tick
      ;; t. Algebraic solution v(t) = v(0)*c^t.
      tracking-braking (atom false)
      braking-started-tick (atom -1)
      start-of-braking-speed (atom -1)
      update-weight (atom 0.75)]
  (defn get-coeff [] @deceleration-coefficient)
  (defn- update-deceleration-coefficient [given-coeff]
    (let [new-coeff (+ (* given-coeff @update-weight)
                       (* @deceleration-coefficient (- 1 @update-weight)))]
      ;; Speedometer works funnily sometimes... Filter crazy values.
      (if (and (not (Double/isNaN new-coeff))
               (< given-coeff 1)
               (or
                (> given-coeff (* 0.5 @deceleration-coefficient))
                (< (* 1.5 @deceleration-coefficient) given-coeff)))
        (do
          (reset! deceleration-coefficient new-coeff)
          ;; on every update we trust the history a bit more.
          (swap! update-weight * 0.8)
          (timbre/debug "got new coeff sample" given-coeff
                        "deceleration coefficient now" @deceleration-coefficient))
        (timbre/warn "Deceleration coefficient was weird:" given-coeff "not updated"))))

  (defn cancel-current-braking-measurement []
    (reset! tracking-braking false))

  (defn braking-started [tick speed]
    (reset! tracking-braking true)
    (reset! braking-started-tick tick)
    (reset! start-of-braking-speed speed))

  (defn braking-ended [tick speed]
    ;; Refine our guess. v(t) = v(0)*c^t => c = e^[ln(v(t)/v(0))/t]
    (when (and @tracking-braking (> @braking-started-tick 0) (> @start-of-braking-speed 0))
      (reset! tracking-braking false)
      (let [braking-time (- tick @braking-started-tick)]
        ;; Ignore short brakings due to "inertia".
        (when (> braking-time 3)
          (let [empirical-coeff
                (Math/pow Math/E
                          (/ (Math/log (/ speed @start-of-braking-speed))
                             braking-time))]
            (update-deceleration-coefficient empirical-coeff))))))

  (defn braking-time-for-speed-difference
    "Calculate the estimated braking time when braking starts at given
    current speed and ends in target speed. Since v[t] = v[0]*c^t => t
    = ln(v[t]/v[0])/ln(c). Don't call with curr-speed 0 or
    target-speed 0, won't return an intelligent value in that
    case. Also when curr-speed < target-speed you'll get a negative
    time."
    [curr-speed target-speed]
    (if (or (< curr-speed 0.001) (< target-speed 0.001))
      Double/MAX_VALUE
      (/ (Math/log (/ target-speed curr-speed))
         (Math/log @deceleration-coefficient))))

  (defn braking-distance-for-speed-difference
    "Calculate estimated braking distance when braking starts at given
    current speed and ends in target speed. Integrate v[0]*c^τ by τ
    from 0 to t (time it takes to decelerate). Integration yields
    [v[0](c^t-1)]/log(c) which is the distance. Curr-speed or
    target-speed shouldn't be 0."
    [curr-speed target-speed]
    (if (or (< curr-speed 0.001) (< target-speed 0.001))
      Double/MAX_VALUE
      (let [braking-time (braking-time-for-speed-difference curr-speed target-speed)]
        (/ (* curr-speed (- (Math/pow @deceleration-coefficient braking-time) 1))
           (Math/log @deceleration-coefficient))))))

(defn- lane-dist-from-center [piece laneidx]
  (let [lanes (:lanes piece)]
    (or (loop [lane (first lanes)
               rest-lanes (rest lanes)]
          (if (= (:index lane) laneidx)
            (:distanceFromCenter lane)
            (recur (first rest-lanes) (rest rest-lanes))))
        (do (timbre/warn "piece" piece "didn't seem to have lane index" laneidx)
            0))))

(defn- radius-of-piece [position piece]
  ;(timbre/debug "piece" piece "radius" (:radius piece) "is bend?" (is-bend? piece))
  (if (is-bend? piece)
    (let [lane (-> position :piecePosition :lane :endLaneIndex)
          radius (+ (:radius piece) (lane-dist-from-center piece lane))]
      radius)
    (do (timbre/warn "Radius called for straight piece!") 1)))

(defn get-target-speed
  "Get target speed for piece in position + piece-offset.

  All straights have unlimited speed. For bends:

  First try and see if we have a speed value in memory.

  Also calculate target speed based on centripetal force F=(mv²)/r,
  friction force F=km, and downforce F=dv² -> v = √[(krm)/(m-d). We
  substitute m with 1. Friction force must be equal to or greater than
  the sum of required centripetal force and downforce.

  If we have a memorized value, it might be from the first lap when we
  didn't have time to accelerate much yet. So, if it much smaller than
  the friction-based value, use friction based value. Otherwise use
  memorized value."
  [pieces position piece-offset]
  (let [current-index (get-current-index position)
        target-index (mod (+ current-index piece-offset) (count pieces))
        target-piece (nth pieces target-index)]
    (if (is-straight? target-piece)
      Double/MAX_VALUE
      (let [memorized-speed (get-piece-speed-memory target-index)
            friction-based-speed
            (Math/sqrt (/ (* @friction (radius-of-piece position target-piece))
                          (- 1 @downforce)))]
        (if (or (not memorized-speed)
                (< memorized-speed (* 0.7 friction-based-speed)))
          friction-based-speed
          memorized-speed)))))

(defn set-friction [new-friction]
  (timbre/debug "Setting friction to" new-friction)
  (reset! friction new-friction))

(defn set-max-angle [new-max-angle]
  (when (> new-max-angle 1)
    (timbre/debug "Setting new max angle to" new-max-angle)
    (reset! max-angle new-max-angle)))

(defn reduce-memorized-piece-speed
  "Should only be called when it's ok to mess with the current piece
speed accumulation, like immediately after crash."
  [index factor]
  (let [prev-speed (get-piece-speed-memory index)]
    (when prev-speed
      (let [new-speed (* factor prev-speed)]
        (timbre/info "setting piece" index "max speed from" prev-speed "to" new-speed)
        (memorize-piece-speed index new-speed)))))

(defn update-friction-after-crash
  "Update the friction value after crash."
  [state race]
  (let [speed (:speed state)
        position (:position state)
        pieceidx (-> position :piecePosition :pieceIndex)
        in-piece-dist (-> position :piecePosition :inPieceDistance)
        pieces (:pieces (:track race))
        piece (nth pieces pieceidx)
        lane (-> position :piecePosition :lane :endLaneIndex)
        piece-length (:length (nth (:lanes piece) lane))]
    ;; Mark not to use this data for piece speed memory
    (mark-piece-speed-entry-invalid)
    ;; Crash might be because of switching lanes. Then reported speed
    ;; and angle is 0.
    (when (> speed 0.1)
      ;; When crashed with speed, angle must be sane! Oh god please
      ;; let it be!
      (set-max-angle (Math/min @max-angle (Math/abs (:angle position))))
      (if (is-bend? piece)
        ;; See how far into the bend we got before crashing. If we
        ;; made it more than three thirds through, trust the
        ;; calculated friction value. Otherwise cut back a bit
        ;; depending how far we got.
        (let [percentage-cleared (/ in-piece-dist piece-length)
              modifier (Math/min (/ percentage-cleared 0.75) 1.0)
              radius (radius-of-piece position piece)
              new-friction (* modifier (/ (Math/pow speed 2) radius))]
          (timbre/info "Crashed on piece with radius" radius "and speed" speed
                       "at point" percentage-cleared
                       ". Updating friction to minimum of a fraction of old"
                       @friction "and new" new-friction)
          (set-friction (Math/min (* 0.95 @friction) new-friction))
          ;; Also fix if we have memorized a speed and now we crashed.
          (reduce-memorized-piece-speed pieceidx 0.5))
        (do
          (timbre/info "Crashed on straight. Updating friction to a fraction of old friction" @friction)
          (set-friction (* 0.95 @friction))
          ;; If previous piece was a bend, update its memorized max
          ;; speed, as obviously we can't come that fast to it next
          ;; time.
          (let [prev-idx (mod (- pieceidx 1) (count pieces))]
            (if (is-bend? (nth pieces prev-idx))
              (reduce-memorized-piece-speed prev-idx 0.5))))))))

(defn update-friction-from-bend [position piece speed]
  (let [radius (radius-of-piece position piece)
        new-friction (/ (Math/pow speed 2) radius)]
    (when (< new-friction @friction)
      (set-friction new-friction))))

(defn get-corners-max-speeds-at-distance
  "Get distances to corners and their max speeds up to the given
  distance from position. Does not return the current track piece the
  car is on in this set."
  [track position distance]
  ;; Todo: dist-to-next-piece could be calculated using
  ;; track/distance, if it were available...
  (let [lane (-> position :piecePosition :lane :endLaneIndex)
        current-index (get-current-index position)
        pieces (:pieces track)
        dist-to-next-piece (- (:length (nth (:lanes (nth pieces current-index)) lane))
                              (-> position :piecePosition :inPieceDistance))]
    (loop [how-far-looking-at-now dist-to-next-piece
           offset 1
           coming-pieces (concat
                          (drop (+ current-index 1) pieces)
                          (take (+ current-index 1) pieces))
           corner-datas (list)]
      (if (and (first coming-pieces)
               (< how-far-looking-at-now distance))
        (recur (+ (:length (nth (:lanes (first coming-pieces)) lane)) how-far-looking-at-now)
               (inc offset)
               (rest coming-pieces)
               (if (is-bend? (first coming-pieces))
                 (conj corner-datas
                       {:distance how-far-looking-at-now
                        :max-speed (get-target-speed pieces position offset)
                        :index (+ offset current-index)})
                 corner-datas))
        (reverse corner-datas)))))

(defn need-to-brake
  "Figure out if we need to start braking."
  [curr-speed dist-max-speed-map turbo-factor]
  (when (not (empty? dist-max-speed-map))
    (timbre/debug "need to brake data" dist-max-speed-map)
    (loop [ds (first dist-max-speed-map)
           ds-list (rest dist-max-speed-map)]
      (if ds
        (let [{d :distance m :max-speed} ds]
          ;; Start braking a bit earlier (one-two ticks) just to be
          ;; sure. 2 ticks is about 2 times the current speed
          ;; (but consider the car might continue accelerating)
          (if (> (+ (braking-distance-for-speed-difference curr-speed m)
                    (* 2 turbo-factor curr-speed)) d)
            (do (timbre/debug "need to brake because of bend distance" d "max speed" m)
                true)
            (recur (first ds-list) (rest ds-list))))
        nil))))

(defn angle-to-throttle-coeff [angle]
  (let [max-throttle-coeff 1.1]
    (Math/max 0.0 (Math/min 1.0 (* max-throttle-coeff (/ (- @max-angle angle) @max-angle))))))

(defn home-straight?
  [race position]
  (if-not (track/final-lap? race position)
    false
    (let [track (:track race)
          current-index (get-current-index position)
          pieces-left (track/get-pieces track current-index true)]
      (empty? (filter :angle pieces-left)))))

(let [curridx (atom -1)]
  (defn get-throttle
    [race my-position their-positions speed turbo-factor]
    (let [track (:track race)
          pieces (:pieces track)

          target-speed (get-target-speed pieces my-position 0)

          ;; braking-distance: assume 0.1 is slow enough speed for
          ;; every curve. This affects the lookahead distance for
          ;; checking when to brake.
          braking-distance (braking-distance-for-speed-difference speed 0.1)

          corner-dists-max-speeds
          (get-corners-max-speeds-at-distance track my-position braking-distance)

          now-on-bend (on-bend? pieces my-position)

          angle (Math/abs (:angle my-position))]

      (when (not= (get-current-index my-position) @curridx)
        (inc-piece-speed-entry @curridx)
        (clear-piece-speed-entry)
        (reset! curridx (get-current-index my-position))
        (timbre/info "Target speed for track piece in idx" @curridx "is" target-speed))

      (if now-on-bend
        (update-piece-speed-entry speed))

      (if (home-straight? race my-position)
        max-throttle
        (if (or (and now-on-bend
                     (and (> angle (* 0.7 @max-angle)) (> speed target-speed))
                     (do (timbre/debug "BRAKING BECAUSE ON BEND GOING TOO FAST") true))
                (and now-on-bend
                     (> angle (* 0.9 @max-angle))
                     (do (timbre/debug "BRAKING BECAUSE ON BEND WITH TOO HIGH ANGLE")
                         (update-friction-from-bend my-position (nth pieces @curridx) speed)
                         true))
                (and (> speed 0)
                     (need-to-brake speed corner-dists-max-speeds turbo-factor)
                     (do (timbre/debug "BRAKING BECAUSE CORNER COMING") true)))
          min-throttle
          ;; If on bend: don't use turbo boost and restrict throttle by
          ;; angle.
          (if now-on-bend
            (* (/ max-throttle turbo-factor) (angle-to-throttle-coeff angle))
            max-throttle))))))
