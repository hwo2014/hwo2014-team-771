; Tehoturskat bot
(ns hwo2014bot.core
  (:require [clojure.data.json :as json]
            [clojure.tools.cli :refer [parse-opts]]
            [taoensso.timbre :as timbre]
            [hwo2014bot.throttle :as throttle]
            [hwo2014bot.state :as state])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (timbre/info (str "Connection closed: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(defmulti handle-msg (fn [msg channel options] (:msgType msg)))

(defmethod handle-msg "yourCar" [msg channel options]
  (state/save-car (:data msg)))

(defmethod handle-msg "gameInit" [msg channel options]
  (state/save-race (-> msg :data :race)))

(defmethod handle-msg "gameStart" [msg channel options]
  (send-message
    channel
    {:msgType "throttle" :data 1.0}))

(defmethod handle-msg "carPositions" [msg channel options]
  (when (:gameTick msg)
    (let [status (state/update-position (:gameTick msg) (:data msg) options)]
      (cond (and (:switch-lane status) (:switch-lane-changed status))
            (do
              (timbre/info "Switching lane to" (:switch-lane status))
              (send-message
                channel
                {:msgType "switchLane" :data (:switch-lane status)}))
            (state/use-turbo?)
            (do
              (timbre/info "Using TURBO!")
              (send-message
                channel
                {:msgType "turbo" :data "havuja prkl!"}))
            :else
            (send-message
              channel
              {:msgType "throttle" :data (:throttle status)})))))

(defmethod handle-msg "turboAvailable" [msg channel options]
  (let [turbo-factor (-> msg :data :turboFactor)
        turbo-duration (-> msg :data :turboDurationTicks)]
    (state/save-turbo true turbo-factor turbo-duration)))

(defmethod handle-msg "crash" [msg channel options]
  (let [name (-> msg :data :name)]
    (if (= name (-> (state/get-car) :name))
      (do
        (timbre/info name "crashed:" (state/get-status))
        (throttle/cancel-current-braking-measurement)
        (throttle/update-friction-after-crash (state/get-status) (state/get-race)))
      (timbre/info name "crashed! XD suckers!"))))

(defmethod handle-msg "spawn" [msg channel options]
  (timbre/info (-> msg :data :name) "spawned"))

(defmethod handle-msg :default [msg channel options]
  (timbre/warn "Unknown command:" (:msgType msg) "data:" (:data msg)))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (timbre/info "Joined")
    "gameStart" (timbre/info "Race started")
    "gameEnd" (timbre/info "Race ended")
    "error" (timbre/error (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel options]
  (let [msg (read-message channel)]
    (log-msg msg)
    (handle-msg msg channel options)
    (recur channel options)))

(def cli-options
  [["-H" "--host HOST" "Remote host"
    :default "testserver.helloworldopen.com"]
   ["-p" "--port PORT" "Port number"
    :default 8091
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-n" "--botname BOTNAME" "Bot name"
    ;; If there are multiple --botname arguments, simply override
    :assoc-fn (fn [m id value] (assoc-in m [id] value))]
   ["-k" "--botkey BOTKEY" "Bot key"]
   [nil "--create" "Create a new race"]
   [nil "--join" "Join an existing race"]
   ["-t" "--track TRACK" "Name of the track to join, optional"]
   [nil "--password PASSWORD" "Password for the race, optional"]
   ["-c" "--cars COUNT" "Count of cars in the race"
    :default 1
    :parse-fn #(Integer/parseInt %)]
   [nil "--debug" "Print debug information on e.g. speed and throttle"]
   ["-f" "--friction FRICTION" "Friction coefficient, default 0.3"
    :default 0.3
    :parse-fn #(Double/parseDouble %)]])

(defn- get-game-options
  [opts]
  {:debug (-> opts :options :debug)
   :friction (-> opts :options :friction)})

(defn- filter-nils [m]
  (into {} (remove (comp nil? val) m)))

(defn -main[& args]
  (let [opts (parse-opts args cli-options)
        host (-> opts :options :host)
        port (-> opts :options :port)
        botname (-> opts :options :botname)
        botkey (-> opts :options :botkey)
        options (get-game-options opts)]
    (cond (-> opts :errors)
          (println (str "ERROR: " (:errors opts) ", usage:\n" (:summary opts)))
          (nil? (-> opts :options :botname))
          (println (str "ERROR: Bot name missing, usage:\n" (:summary opts)))
          (nil? (-> opts :options :botkey))
          (println (str "ERROR: Bot key missing, usage:\n" (:summary opts)))
          (and (-> opts :options :create) (-> opts :options :join))
          (println (str "ERROR: Cannot create and join at the same time, usage:\n" (:summary opts)))
          :else
          (let [channel (connect-client-channel host port)
                msg (cond (-> opts :options :create)
                          {:msgType "createRace" :data (filter-nils
                                                         {:botId {:name botname
                                                                  :key botkey}
                                                          :trackName (-> opts :options :track)
                                                          :password (-> opts :options :password)
                                                          :carCount (-> opts :options :cars)})}
                          (-> opts :options :join)
                          {:msgType "joinRace" :data (filter-nils
                                                       {:botId {:name botname
                                                                :key botkey}
                                                        :trackName (-> opts :options :track)
                                                        :password (-> opts :options :password)
                                                        :carCount (-> opts :options :cars)})}
                          :else
                          {:msgType "join" :data {:name botname :key botkey}})]
            (throttle/set-friction (:friction options))
            (timbre/info "Sending join message:" msg)
            (send-message channel msg)
            (timbre/spy options)
            (if (:debug options)
              (game-loop channel options)
              (timbre/with-log-level :info
                (game-loop channel options)))))))
