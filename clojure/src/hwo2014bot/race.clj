(ns hwo2014bot.race)

(defn piece-lane-length
  [piece lane]
  (or
   (:length piece)
   (let [angle (:angle piece)
         radiusmod (if (neg? angle) + -)
         radius (radiusmod (:radius piece) (:distanceFromCenter lane))]
     (Math/abs (/ (* Math/PI radius angle) 180)))))

(defn calculate-curve
  [angle length]
  (if (and angle (> length 0)) (/ angle length) 0))

(defn process-lane
  [piece lane]
  (let [length (piece-lane-length piece lane)
        curve (calculate-curve (:angle piece) length)]
    (merge lane {:length length, :curve curve})))

(defn process-piece
  [piece lanes]
  (merge piece {:lanes (map #(process-lane piece %) lanes)}))

(defn process-track
  [track]
  (let [pieces (map #(process-piece % (:lanes track)) (:pieces track))]
    (merge track {:pieces pieces})))

(defn process
  [race]
  (let [track (process-track (:track race))]
    (merge race {:track track})))
